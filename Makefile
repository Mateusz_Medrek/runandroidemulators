NAME=runandroidemulators
DOMAIN=mateusz1913.dev
PACKAGE_NAME=dev.mateusz1913.runandroidemulators

.PHONY: all pack install clean lint format prepare-translations

all: node_modules

node_modules: package.json
	npm install

prepare-translations:
	@xgettext --from-code=UTF-8 \
		--output=po/$(NAME)@$(DOMAIN).pot *.js \
		--package-name=$(PACKAGE_NAME)

$(NAME)@$(DOMAIN).shell-extension.zip: all
	@gnome-extensions pack --extra-source=constants.js \
		--extra-source=utils.js \
		--extra-source=icons \
		--extra-source=LICENSE \
		--extra-source=README.md \
		--force \
		.

pack: $(NAME)@$(DOMAIN).shell-extension.zip

lint: node_modules
	@(npm run lint)

format: node_modules
	@(npm run format)

install: pack
	@(gnome-extensions install --force $(NAME)@$(DOMAIN).shell-extension.zip)

clean:
	@rm -rf dist node_modules $(NAME)@$(DOMAIN).shell-extension.zip

